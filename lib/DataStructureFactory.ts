import {DataStructure} from "./DataStructure";
import {Queue} from "./Queue";
import {Stack} from "./Stack";

export class DataStructureFactory {
    public static create(dataStructure: DataStructure) {
        switch (dataStructure) {
            case DataStructure.Queue:
                return new Queue();
            case DataStructure.Stack:
                return new Stack();
        }
    }
}
