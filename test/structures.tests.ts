import { expect } from "chai";
import { Queue } from "../lib/Queue";
import {DataStructure} from "../lib/DataStructure";
import {DataStructureFactory} from "../lib/DataStructureFactory";
import {Stack} from "../lib/Stack";

describe("Data structures", () => {
    describe("Factory", () => {
        it("can create Queue", () => {
            const result = DataStructureFactory.create(DataStructure.Queue);
            expect(result).to.be.an.instanceOf(Queue);
        });

        it("can create Stack", () => {
            const result = DataStructureFactory.create(DataStructure.Stack);
            expect(result).to.be.an.instanceOf(Stack);
        });
    });

    describe("Queue", () => {
        it("has size", () => {
            const testee = new Queue();
            const result = testee.size();
            expect(result).to.equal(0);
        });

        it("has item", () => {
            const testee = new Queue();
            testee.enqueue("first");
            const result = testee.size();
            expect(result).to.equal(1);
        });

        it("has enqueued item", () => {
            const enqueuedItem = "first";
            const testee = new Queue();
            testee.enqueue(enqueuedItem);
            const result = testee.peek();
            expect(result).to.equal(enqueuedItem);
        });

        it("can remove first item", () => {
            const enqueuedItem = "first";
            const testee = new Queue();
            testee.enqueue(enqueuedItem);
            testee.enqueue("second");
            const result = testee.poll();
            expect(result).to.equal(enqueuedItem);
            expect(testee.size()).to.equal(1);
        });
    });

    describe("Stack", () => {
        it("has size", () => {
            const testee = new Stack();
            const result = testee.size();
            expect(result).to.equal(0);
        });

        it("has item", () => {
            const testee = new Stack();
            testee.enqueue("first");
            const result = testee.size();
            expect(result).to.equal(1);
        });

        it("has enqueued item", () => {
            const testee = new Stack();
            const enqueuedItem = "first";
            testee.enqueue(enqueuedItem);
            const result = testee.peek();
            expect(result).to.equal(enqueuedItem);
        });

        it("can remove last added item", () => {
            const testee = new Stack();
            const enqueuedItem = "second";
            testee.enqueue("first");
            testee.enqueue(enqueuedItem);
            const result = testee.poll();
            expect(result).to.equal(enqueuedItem);
            expect(testee.size()).to.equal(1);
        });
    });

});
